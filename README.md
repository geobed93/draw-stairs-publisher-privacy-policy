# Publisher Privacy Policy

This Privacy Policy is designed to help you understand how the App, collects, uses, and safeguards any information that you provide when using the App. We take your privacy seriously and are committed to ensuring that any information collected is handled responsibly and in accordance with this policy.

### a. Data Collection and Usage:
The App does not collect any data from its users. We do not gather personally identifiable information, device information, or any other data during your use of the App. Therefore, there is no information to describe regarding data collection, as no data is collected.

### b. Third-Party Data Sharing:
Since the App does not collect any data, there is no information to share with third parties. We do not engage with any analytics tools, advertising networks, third-party SDKs, or legal affiliates for the purpose of sharing user data.

### c. Data Retention and Deletion Policies:
As the App does not collect any data, we do not have any data retention policies. No information is stored, and therefore, there is no data to retain or delete.

### d. Consent Revocation and Data Deletion Requests:
Users of the App do not need to worry about revoking consent or requesting data deletion since the App does not collect any user data. As such, there is no need for users to take any action related to consent or data deletion.

### Contact Information:
If you have any questions or concerns regarding this Privacy Policy, please contact us at gbadran_90@live.com.

### Changes to the Privacy Policy:
The App reserves the right to update or modify this Privacy Policy at any time. Users will be notified of any significant changes through in-app notifications or other means. It is advisable to review this Privacy Policy periodically for any updates.
